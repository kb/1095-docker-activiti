Run Activiti with Tomcat in Docker
**********************************

.. highlight:: bash

Download ``activiti-5.18.0.zip`` from http://activiti.org/download.html

Open the ``zip`` file and extract ``activiti-explorer.war`` from the
``/activiti-5.18.0/wars/`` folder.

Copy ``activiti-explorer.war`` to this folder (the same folder as
``README.rst``).

Install Docker! https://docs.docker.com/installation/ubuntulinux/

Note: After install and before verifying docker is installed correctly with ``sudo docker run hello-world`` run::

  sudo service docker start

Build the Docker image::

  docker build -t mytomcat .

Run::

  docker run -d -p 8080:8080 mytomcat

Browse to http://localhost:8080/activiti-explorer/ and log in::

  user      pass        role
  --------  ----------  -------
  kermit    kermit      admin
  gonzo     gonzo       manager
  fozzie    fozzie      user

For more information browse to: ``/docs/userguide/index.html#10minutetutorial``
in the ``zip`` file.

Find the ID of the process::

  docker ps

Follow the logs::

  docker logs -f 5cf9d1593c2f